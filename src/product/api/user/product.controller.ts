import {Controller, UseGuards} from "@nestjs/common";
import {Crud, CrudController} from "@nestjsx/crud";
import {Product} from "../../entity/product.entity";
import {ProductDto} from "./dto/product.dto";
import {ApiBearerAuth, ApiOkResponse} from "@nestjs/swagger";
import {getManyResponseDecorator} from "../../../../lib/utils/pagination.util";
import {AuthGuard} from "@nestjs/passport";
import {ProductProvider} from "../../service/product.provider";

@Controller('/api/product')
@Crud({
  params: {
    id: {
      type: 'uuid',
      primary: true,
      field: 'id',
    },
  },
  query: {
    cache: 1000 * 60, // 1 minute
    alwaysPaginate: true
  },
  routes: {
    exclude: [
      'updateOneBase',
      'recoverOneBase',
      'createManyBase'
    ],
    getManyBase: {
      decorators: [
        ApiOkResponse({ type: getManyResponseDecorator(Product) })
      ],
    },
    createOneBase: {
      decorators: [
        UseGuards(AuthGuard()),
        ApiBearerAuth(),
      ]
    },
    deleteOneBase: {
      decorators: [
        UseGuards(AuthGuard()),
        ApiBearerAuth(),
      ]
    },
    replaceOneBase: {
      decorators: [
        UseGuards(AuthGuard()),
        ApiBearerAuth(),
      ]
    },
  },
  dto: {
    create: ProductDto,
    update: ProductDto,
    replace: ProductDto,
  },
  model: {
    type: Product
  },
})
export class ProductController implements CrudController<Product> {
  constructor(public service: ProductProvider) {
  }
  get base(): CrudController<Product> {
    return this;
  }
}