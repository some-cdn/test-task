import {OmitType} from "@nestjs/swagger";
import {Product} from "../../../entity/product.entity";

export class ProductDto extends OmitType(Product, ['id']) {

}